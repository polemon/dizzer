#! /usr/bin/env python3
# formulas for this file from:
# http://www.brucelindbloom.com

import numpy as np
import contextlib
import math

#############
# CONSTANTS
#########

# sRGB to CIE 1931 XYZ transformation matrix
# observer: 2°, illuminant: D65
M = np.array( [ [ 0.4124564, 0.3575761, 0.1804375 ],
                [ 0.2126729, 0.7151522, 0.0721750 ],
                [ 0.0193339, 0.1191920, 0.9503041 ] ] )

# inverse M
M_inv = np.linalg.inv(M)

# Reference white for D65 illuminant:
W = np.array( [ 0.95047, 1.00000, 1.08883 ] )

# Epsilon fron the CIE standard
# epsilon = 0.008856
epsilon = 216 / 24389

# Kappa from the CIE standard
# kappa = 903.3
kappa = 24389 / 27


#############
# FUNCTIONS
#########

# RGB SCALING
#############

# normalize integer RGB triplet to unsigned float
def intrgb2floatrgb(rgb = [ 255, 255, 255 ], depth = 8):
    return np.array(rgb) / (2**depth - 1)

# rescale float RGB to unsigned int RGB (clip to [ 0 ; maxdepth ])
def floatrgb2intrgb(rgb = [ 1.0, 1.0, 1.0 ], depth = 8):
    expanded_triplet = np.array(rgb) * (2**depth - 1)
    int_triplet = np.rint(expanded_triplet).astype(int)
    return np.clip(int_triplet, a_min = 0, a_max = 2**depth - 1)


# sRGB <--> CIEXYZ
##################

# convert sRGB to XYZ.
def rgb2xyz(rgb):
    # Inverse sRGB companding
    lin_rgb = np.array(list(map( lambda v: ((v + 0.055) / 1.055)**2.4 if v > 0.04045 else v / 12.92, rgb )))

    # Linear RGB to XYZ conversion
    xyz = M @ lin_rgb

    return xyz

# convert XYZ to sRGB
def xyz2rgb(xyz):
    # XYZ to linear RGB conversion
    lin_rgb = M_inv @ xyz

    # sRGB companding
    rgb = np.array(list(map( lambda v: 12.92 * v if v <= 0.0031308 else 1.055 * v**(1/2.4) - 0.055, lin_rgb )))

    # clip to [ 0 ; 1 ]
    return np.clip(rgb, a_min = 0, a_max = 1)


# CIEXYZ <--> CIEL*a*b*
#######################

# convert XYZ to L*a*b*.
def xyz2lab(xyz):
    # normalize using reference white D65
    xr, yr, zr = (xyz / W)

    # nonlinear transformation function
    f_v = lambda v: math.cbrt(v) if v > epsilon else ( (kappa * v) + 16 ) / 116

    # conversion
    L = 116 * f_v(yr) - 16
    a = 500 * ( f_v(xr) - f_v(yr) )
    b = 200 * ( f_v(yr) - f_v(zr) )

    return [ L, a, b ]

# convert L*a*b* to XYZ
def lab2xyz(lab):
    L, a, b = lab

    # nonlinear transformation functions
    fy = (L + 16) / 116
    fx = a / 500 + fy
    fz = fy - b / 200

    # conversion
    xr = fx**3               if fx**3 > epsilon     else (116 * fx - 16) / kappa
    yr = ((L + 16) / 116)**3 if L > kappa * epsilon else L / kappa
    zr = fz**3               if fz**3 > epsilon     else (116 * fz - 16) / kappa

    # scale using reference white D65
    return (np.array([ xr, yr, zr ]) * W)


# XYZ <--> L*u*v*
#################

# convert XYZ to L*u*v*
def xyz2luv(xyz):
    Xr, Yr, Zr = W
    x, y, z = xyz

    ur_prime = (4 * Xr) / (Xr + 15 * Yr + 3 * Zr)
    vr_prime = (9 * Yr) / (Xr + 15 * Yr + 3 * Zr)

    u_prime = (4 * x) / (x + 15 * y + 3 * z) if x else 0
    v_prime = (9 * y) / (x + 15 * y + 3 * z) if y else 0

    yr = y / Yr

    L = 116 * math.cbrt(yr) - 16 if yr > epsilon else kappa * yr
    u = 13 * L * (u_prime - ur_prime)
    v = 13 * L * (v_prime - vr_prime)

    return [ L, u, v ]

# convert L*u*v* to XYZ
def luv2xyz(luv):
    Xr, Yr, Zr = W
    L, u, v = luv

    u_0 = (4 * Xr) / (Xr + 15 * Yr + 3 * Zr)
    v_0 = (9 * Yr) / (Xr + 15 * Yr + 3 * Zr)

    y = ( (L + 16) / 116 )**3 if L > kappa * epsilon else L / kappa

    a = (1/3) * ( (52 * L) / (u + 13 * L * u_0) - 1 ) if L else 0
    b = -5 * y
    c = -1/3
    d = y * ( (39 * L) / (v + 13 * L * v_0) - 5 ) if L else 0

    x = (d - b) / (a - c)
    z = x * a + b

    return [ x, y, z ]


# L*a*b* <--> L*C*h
###################

# convert L*a*b* to L*C*h
def lab2lch(lab):
    L, a, b = lab

    C = math.sqrt(a**2 + b**2)
    h = np.arctan2(b, a)

    return [ L, C, h ]

# convert L*C*h to L*a*b*
def lch2lab(lch):
    L, C, h = lch

    a = C * np.cos(h)
    b = C * np.sin(h)

    return [ L, a, b ]


# L*u*v* <--> L*C*h
###################

# convert L*u*v* to L*C*h
# NOTE not a bug. Same mathematical algorithms.
def luv2lch(luv):
    return lab2lch(luv)

# convert L*C*h to L*u*v*
# NOTE not a bug. Same mathematical algorithms.
def lch2luv(lch):
    return lch2lab(lch)

# Currying
##########

# RGB --> XYZ --> L*a*b
def rgb2lab(rgb):
    return xyz2lab(rgb2xyz(rgb))

# RGB --> XYZ --> L*u*v*
def rgb2luv(rgb):
    return xyz2luv(rgb2xyz(rgb))

# L*a*b* --> XYZ --> RGB
def lab2rgb(lab):
    return xyz2rgb(lab2xyz(lab))

# L*u*v* --> XYZ --> RGB
def luv2rgb(luv):
    return xyz2rgb(luv2xyz(luv))

# RGB --> XYZ --> L*a*b* --> L*C*h(ab)
def rgb2lch_ab(rgb):
    return lab2lch(rgb2lab(rgb))

# RGB --> XYZ --> L*u*v* --> L*C*h(uv)
def rgb2lch_uv(rgb):
    return luv2lch(rgb2luv(rgb))

# L*C*h(ab) --> L*a*b* --> XYZ --> RGB
def lch_ab2rgb(lch):
    return lab2rgb(lch2lab(lch))

# L*C*h(uv) --> L*u*v* --> XYZ --> RGB
def lch_uv2rgb(lch):
    return luv2rgb(lch2luv(lch))


def test(name = "(none)", c = [ 0x00, 0x00, 0x00 ]):
    print(f" »»» {name} ", np.vectorize(hex)(c))
    frgb = intrgb2floatrgb(rgb = c, depth = 8)
    print("   RGB (float): ", np.vectorize("%.5f".__mod__)( intrgb2floatrgb(rgb = c, depth = 8) ))
    print("   RGB (int):   ", np.vectorize("%.5f".__mod__)( floatrgb2intrgb(rgb = frgb, depth = 8) ))
    print("   XYZ:         ", np.vectorize("%.5f".__mod__)( rgb2xyz(frgb) ))
    print("   Lab:         ", np.vectorize("%.5f".__mod__)( rgb2lab(frgb) ))
    print("   Luv:         ", np.vectorize("%.5f".__mod__)( rgb2luv(frgb) ))
    print("   LCh(ab):     ", np.vectorize("%.5f".__mod__)( rgb2lch_ab(frgb) ))
    print("   LCh(uv):     ", np.vectorize("%.5f".__mod__)( rgb2lch_uv(frgb) ))
    print(" »»» round trips")
    print("   RGB -> XYZ -> RGB: ", np.vectorize("%.5f".__mod__)( xyz2rgb(rgb2xyz(frgb)) ))
    print("   RGB -> XYZ -> Lab -> XYZ -> RGB: ", np.vectorize("%.5f".__mod__)( lab2rgb(rgb2lab(frgb)) ))
    print("   RGB -> XYZ -> Luv -> XYZ -> RGB: ", np.vectorize("%.5f".__mod__)( luv2rgb(rgb2luv(frgb)) ))
    print("   RGB -> XYZ -> Lab -> LCh(ab) -> Lab -> XYZ -> RGB: ", np.vectorize("%.5f".__mod__)( lch_ab2rgb(rgb2lch_ab(frgb)) ))
    print("   RGB -> XYZ -> Luv -> LCh(uv) -> Luv -> XYZ -> RGB: ", np.vectorize("%.5f".__mod__)( lch_uv2rgb(rgb2lch_uv(frgb)) ))
    print(" «««")
    print()

    return


def main():
    test(name = "black",        c = [ 0x00, 0x00, 0x00 ])
    test(name = "red",          c = [ 0xFF, 0x00, 0x00 ])
    test(name = "green",        c = [ 0x00, 0xFF, 0x00 ])
    test(name = "blue",         c = [ 0x00, 0x00, 0xFF ])
    test(name = "yellow",       c = [ 0xFF, 0xFF, 0x00 ])
    test(name = "magenta",      c = [ 0xFF, 0x00, 0xFF ])
    test(name = "cyan",         c = [ 0x00, 0xFF, 0xFF ])
    test(name = "white",        c = [ 0xFF, 0xFF, 0xFF ])
    test(name = "50% gray",     c = [ 0x7F, 0x7F, 0x7F ])
    test(name = "int. orange",  c = [ 0xFF, 0x4F, 0x00 ])
    test(name = "dark orange",  c = [ 0xD5, 0x6A, 0x00 ])

    return 0

if __name__ == "__main__":
    exit(main())

# Ranges:
# L* : [    0 ;  100 ]
# u* : [ -134 ; +224 ]
# v* : [ -140 ; +122 ]
# a* : [  -86 ;  +98 ]
# b* : [ -107 ;  +94 ]
# C(ab) : [ 0 ; 145.5 ]
# C(uv) : [ 0 ; 264.6 ]
# X : [ 0 ; 0.95047 ]
# Y : [ 0 ; 1 ]
# Z : [ 0 ; 1.08883 ]
